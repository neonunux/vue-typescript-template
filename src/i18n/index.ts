import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from './messages.json'

Vue.use(VueI18n)

export default new VueI18n({
  locale: detectPreferredLocale(messages, 'en'),
  messages
})

function detectPreferredLocale (messages: Object, defaultLocale: string): string {
  return getUserLocales().find(locale => locale in messages) || defaultLocale
}

function getUserLocales (): string[] {
  return removeDuplicates([
    ...(navigator.languages || []),
    navigator.language
  ].filter(locale => !!locale)
   .map(locale => locale.split('-')[0])
  )
}

function removeDuplicates (array: any[]): any[] {
  return [...new Set(array)]
}
