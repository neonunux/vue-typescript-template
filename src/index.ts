import 'static/theme.sass'

import Vue from 'vue'
import i18n from '~/i18n'
import Component from '~/SampleComponent'

const app =  new Vue({
  el: '#application',
  render: createElement => createElement(Component),
  i18n
})
