const webpack = require('webpack')
const config = require('../webpack.config')
const ora = require('ora')

var spinner = ora('building for production...')
spinner.start()

webpack(config, (error, stats) => {
	if (error) {
		console.error(error)
		return
	}
	process.stdout.write(stats.toString({
		colors: true,
		chunks: false
	}) + '\n\n')
	spinner.stop()
})
