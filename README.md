# vue-typescript-template

> A template project using Vue & Typescript


``` bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
yarn run dev

# build for production with minification
# modify to ie dist 
assetsPublicPath: '/',

# then
yarn run build
```

- [vue-typescript-template](https://framagit.org/neonunux/vue-typescript-template)

# TODO

- break everything
